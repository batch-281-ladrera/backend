// Array methods

// Mutator methods:

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current Array:");
console.log(fruits);

//push()
// Adds an element in the end of an array AND returns the array's length
// Syntax: arrayName.push()

fruits.push("Mango");
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method.");
console.log(fruits);

// pop()
/* 
    -Removes the last element in an array AND returns the removed element
    Syntax: arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method.");
console.log(fruits);

// unshift
/* 
    - Adds one or more elements at the beginning of an array
    Syntax: arrayName.unshift('elementA')
    arrayName.unshift('elementA', 'elementB')
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method.");
console.log(fruits);

//shift
/* 
    -Removes an element at the beginning of an array AND returns the removed element
    Syntax:
    arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
/* 
- Simultaneously removes elements from a specified index and adds elements
Syntax:
arrayName.splice(startingIndex, deleteCount)

*/

fruits.splice(2, 3, "Calamansi", "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort ()
/* 
- Rearranges the array elements in alphanumeric order
Syntax: arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
/* 
- Reverses the order of array elements
Syntax: arrayName.reverse()
*/

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
/* 
    Non-mutator methods are functions that do not modify or change an array after they're created

    These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array
*/
let countries = ["US", "PH", "CAN", "SG", "PH", "FR", "GE", "CH", "KR"];

// indexOf('element')
/* 
    Returns the index number of the first matching element found in an array.
    If no match was found, the result will be -1
    The search procss will be done from first element proceeding to the last element.
    Syntax:
        - arrayName.indexOf(searchValue)
        - arrayName.indexOf(searchValue, fromIndex)
 */

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

// lastIndexOf()
/* 
    - Returns the index number of the last matching element found in an array
    - The search process will be done from last element proceeding to first element
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 3);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
/* 
    - Portion/slices elements from an array AND returns a new array
    Syntax:
    arrayName.slice(startingIndex)
    arrayName.slice(startingIndex, endingIndex)

*/

let slicedArrayA = countries.slice(2);
console.log("Resul from slice method: ");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method: ");
console.log(slicedArrayB);

// Slicing off elements from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log("Result from slice method: ");
console.log(slicedArrayC);

//toString();
// Returns an array as string separated by commas
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);

// concat
// Combines 2 arrays and returns the combined result

let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method: ");
console.log(tasks);

let tasksTwo = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result from concat method: ");
console.log(tasksTwo);

let tasksThree = tasksArrayA.concat("smelll express", "throw react");
console.log("Result from concat method: ");
console.log(tasksThree);

//join ()
// Returns an array as string separated by specified separator string
let users = ["John", "Juan", "Dudong"];
console.log(users.join(" - "));

// Iteration methods
/*
        - Iteration methods are loops designed to perform repetitive tasks on arrays
        - Iteration methods loops over all items in an array.
        - Useful for manipulating array data resulting in complex tasks
        - Array iteration methods normally work with a function supplied as an argument
        - How these function works is by performing tasks that are pre-defined within an array's method.
*/

// forEach()
/*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
*/

tasksTwo.forEach(function (task) {
  console.log(task);
});

// Using foreach() for filtering all elements

// map()
// forEach()
/*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function (number) {
  return number * number;
});

console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

//every
//checks if all statement in an array meet the given condition
let allValid = numbers.every(function (number) {
  return number < 3;
});
console.log(allValid);
// some
// checks if some statement in an array meet the given condition
let someValid = numbers.some(function (number) {
  return number < 2;
});
console.log(someValid);

// filter()
/* 
    Returns new array that contains elements which meets the given condition
*/

let filterValid = numbers.filter(function (number) {
  return number < 2;
});

console.log(filterValid);

// include()
// include method checks if the argument passed can be found in the array

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1); // true

let productFound2 = products.includes("Headset");
console.log(productFound2); // false

/*
        - Methods can be "chained" using them one after another
        - The result of the first method is used on the second method until all "chained" methods have been resolved
        - How chaining resolves in our example:
            1. The "product" element will be converted into all lowercase letters
            2. The resulting lowercased string is used in the "includes" method
    */
let filteredProducts = products.filter(function (product) {
  return product.toLowerCase().includes("a");
});

console.log(filteredProducts);

// reduce()
/* 
- Evaluates elements from left to right and returns the array into a single value
Syntax:
let/const resultArray = arrayName.reduce(accumulator, currentValue){
    return expression/operation
}
- The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elemen
----
 - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
===================
- The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
- How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
*/

let iteration = 0;
let reducedArray = numbers.reduce(function (acc, cur) {
  console.log("current iteration: " + ++iteration);
  console.log("accumulator: " + acc);
  console.log("current value: " + cur);
});

console.log(reducedArray);
