// Crud Operations

// Create

db.users.insertOne({
  "firstName": "Jane",
  "lastName": "Doe",
  "age": 21,
  "contact": {
    "phone": "85479123",
    "email": "janedoe@mail.com"
  },
  "course": ["CSS", "JavaScript", "Python"],
  "department": "none"
});

// Insert many collections
db.users.insertMany([
  {
  "firstName": "Stephen",
  "lastName": "Hawking",
  "age": 21,
  "contact": {
    "phone": "85479123",
    "email": "stephen@mail.com"
  },
  "course": ["React", "PHP", "Python"],
  "department": "none"
  },
  {
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age": 82,
    "contact": {
      "phone": "85479123",
      "email": "neilarms@mail.com"
    },
    "course": ["CSS", "JavaScript", "Sass"],
    "department": "none"
    }
]
)

// Finding documents (Read)
// Find

// Retrieving all documents
db.users.find();

// Retrieving single documents
db.users.find({"firstName": "Stephen"});

// Retrieving documents with multiple parameters
db.users.find({"lastName": "Armstrong", "age": 82});

// Updating documents (Update)

// Update a single document to update

// Creating a document to update
db.users.insertOne({
  "firstName": "Test",
  "lastName": "Test",
  "age": 0,
  "contact": {
    "phone": "00000000",
    "email": "test@mail.com"
  },
  "course": [],
  "department": "none"
})

db.users.updateOne(
  {"firstName": "Test"},
  {
    $set : {
      "firstName": "Bill",
      "lastName": "Gates",
      "age": 65,
      "contact": {
        "phone": "87654321",
        "email": "bill@mail.com"
      },
      "course": ["PHP", "Laravel", "HTML"],
      "department": "Operations",
      "status": "active"
    }
  }
)

// Updating multiple documents
db.users.updateMany(
  {"department": "none"},
  {
    $set : {
      "department": "HR"
    }
  }
)

// Replace one
db.users.replaceOne(
  {"firstName": "Bill"},
  {
    "firstName": "Bill",
    "lastName": "Gates",
    "age": 65,
    "contact": {
      "phone": "87654321",
      "email": "bill@mail.com"
    },
    "course": ["PHP", "Laravel", "HTML"],
    "department": "Operations",
  }
)

// Deleting documents (Delete)
db.users.deleteOne(
  {"firstName": "Test"}
  )

// Query an embedded document
db.users.find({
  "contact": {
    "phone": "87654321"
  }
})