const express = require("express");

const app = express();

const port = 4000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

let users = [];

app.get("/home", (req, res) => {
  res.send("Welcome to the home page");
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.post("/signup", (req, res) => {
  if (req.body.username !== "" && req.body.password !== "") {
    users.push(req.body);

    res.send(`User ${req.body.username} successfully registered`);
  } else {
    res.send("Please input BOTH username and password");
  }
});

app.delete("/delete-user", (req, res) => {
  let message;
  if (users.length != 0) {
    for (let i = 0; i < users.length; i++) {
      if (users[i].username == req.body.username) {
        users.splice(i, 1);
        message = `User ${req.body.username} has been deleted`;
        res.send(message);
        break;
      }
    }
    if (req.body.username == undefined) {
      message = "User does not exist.";
      res.send(message);
    }
  } else {
    message = "No users found";
    res.send(message);
  }
});

if (require.main === module) {
  app.listen(port, () => console.log(`Server is running at port ${port}`));
}

module.exports = app;
