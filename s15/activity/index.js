console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "swimming"];
let workAddress = {
  houseNumber: 32,
  street: "Washington",
  city: "Lincoln",
  state: "Nebraska",
};
console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`);
console.log(`Age: ${age}`);
console.log(`My full name is ${firstName} ${lastName}`);
console.log("Hobbies: ");
console.log(hobbies);

firstName = "Steve";
lastName = "Rogers";
console.log(`My full name is ${firstName} ${lastName}`);
age = 40;
console.log(`My current age is: ${age}`);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log(`My friends are:`);
console.log(friends);

let fullProfile = {
  username: "captain_america",
  fullName: "Steve Rogers",
  age: 40,
  isActive: false,
};

console.log(`My Full Profile`);
console.log(fullProfile);

let bestFriend = "Bucky Barnes";

console.log(`My bestfriend is ${bestFriend}`);

let locationFound = "Arctic Ocean";
console.log(`I was found frozen in: ${locationFound}`);
