console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names.
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
  // Initialize/add the given object properties and methods

  // Properties
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
  // Methods

  talk: function () {
    console.log("Pikachu! I choose you!");
  },
};
console.log(trainer);

// Check if all properties and methods were properly added

// Access object properties using dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

// Access object properties using square bracket notation

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Access the trainer "talk" method
console.log("Result of talk method");
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level, health, attack) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  this.tackle = function (target) {
    target.health = target.health - this.attack;
    console.log(`${this.name} tackled ${target.name}`);
    console.log(`${target.name}'s health is now reduced to ${target.health}`);
    if (target.health <= 0) {
      target.faint(target);
    }
  };
  this.faint = function (target) {
    console.log(`${target.name} fainted`);
  };
}
// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pickachu", 12, 24, 12);
console.log(pikachu);
// Create/instantiate a new pokemon
let geodude = new Pokemon("Geodude", 8, 16, 8);
console.log(geodude);
// Create/instantiate a new pokemon
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);
console.log(mewtwo);
// Invoke the tackle method and target a different object
geodude.tackle(pikachu);

// Invoke the tackle method and target a different object

mewtwo.tackle(geodude);
console.log(geodude);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== "undefined" ? trainer : null,
    Pokemon: typeof Pokemon !== "undefined" ? Pokemon : null,
  };
} catch (err) {}
