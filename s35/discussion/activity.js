const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect(
  "mongodb+srv://vinceladrerapersonal:VujCXZ7Lc20SZF8M@wdc028-course-booking.sif1cwb.mongodb.net/b281_to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Set notification for connection success or failure
let db = mongoose.connection;

// If a connection error occurred, output in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas
const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

// Models [Task models]
const User = mongoose.model("User", userSchema);

// Allows the app to read json data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({ extended: true }));

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }).then((result, err) => {
    // If a user was found and user's username matches the information from the client
    if (result != null && result.username == req.body.username) {
      // Return a message to the client/Postman
      return res.send("Duplicate user found");
    }
    // If no same user was found
    else {
      // Create a new user and save it to the database
      let newUser = new User({
        username: req.body.username,
        password: req.body.password,
      });

      newUser.save().then((savedUser, saveErr) => {
        // If there are errors in saving
        if (saveErr) {
          return console.error(saveErr);
        }
        // No error found while creating user
        else {
          return res.status(201).send("New user created");
        }
      });
    }
  });
});

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
