console.log("Hello World");

function login(username, password, role) {
  if (
    username === "" ||
    password === "" ||
    role === "" ||
    username === undefined ||
    password === undefined ||
    role === undefined
  ) {
    return "Inputs must not be empty";
  } else {
    switch (role) {
      case "admin":
        return "Welcome back to the class portal, admin!";
        break;
      case "teacher":
        return "Thank you for logging in, teacher!";
        break;
      case "student":
        return "Welcome to the class portal, student!";
        break;
      default:
        return "Role out of range.";
        break;
    }
  }
}

let username = prompt("Input username");
let password = prompt("Input password");
let role = prompt("Input role");

console.log(login(username, password, role));

let quizOne = parseInt(prompt("Input score for quiz one"));
let quizTwo = parseInt(prompt("Input score for quiz two"));
let quizThree = parseInt(prompt("Input score for quiz three"));
let quizFour = parseInt(prompt("Input score for quiz Four"));

function checkAverage(quizOne, quizTwo, quizThree, quizFour) {
  let average = Math.round(((quizOne + quizTwo + quizThree + quizFour) / 400) * 100);
  if (average <= 74) {
    return `Hello student, your average is: ${average}. The letter equivalent is F`;
  } else if (average >= 75 && average <= 79) {
    return `Hello student, your average is: ${average}. The letter equivalent is D`;
  } else if (average >= 80 && average <= 84) {
    return `Hello student, your average is: ${average}. The letter equivalent is C`;
  } else if (average >= 85 && average <= 89) {
    return `Hello student, your average is: ${average}. The letter equivalent is B`;
  } else if (average >= 90 && average <= 95) {
    return `Hello student, your average is: ${average}. The letter equivalent is A`;
  } else if (average >= 96) {
    return `Hello student, your average is: ${average}. The letter equivalent is A+`;
  }
}

console.log(checkAverage(quizOne, quizTwo, quizThree, quizFour));
