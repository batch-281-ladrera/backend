console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function functionOne() {
  let fullName = prompt("What is your full name?");
  let age = prompt("What is your age?");
  let location = prompt("Input location");
  console.log(`Hello ${fullName}`);
  console.log(`You are ${age} years old.`);
  console.log(`You live in ${location}`);
}

functionOne();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:

function functionTwo() {
  let favoriteBands = [
    "The Juans",
    "Parokya ni Edgar",
    "Ebe dencel",
    "Silent Sanctuary",
    "Ben&Ben",
  ];
  for (let i = 0; i < 5; i++) {
    console.log(`${i + 1}. ${favoriteBands[i]}`);
  }
}
functionTwo();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:

let movieOne = {
  name: "Knives Out",
  rottenTomato: "97%",
};
let movieTwo = {
  name: "The Dark Knight(2008)",
  rottenTomato: "94%",
};
let movieThree = {
  name: "The Dark Knight Rises(2012)",
  rottenTomato: "90%",
};
let movieFour = {
  name: "Seven (1995)",
  rottenTomato: "82%",
};
let movieFive = {
  name: "Shutter Island",
  rottenTomato: "68%",
};

let movieContainer = [movieOne, movieTwo, movieThree, movieFour, movieFive];
function functionThree() {
  for (let i = 0; i < 5; i++) {
    console.log(`${i + 1}. ${movieContainer[i].name}`);
    console.log(`Rotten Tomato Rating: ${movieContainer[i].rottenTomato}`);
  }
}

functionThree();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function () {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");
  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();

//console.log(friend1);
//console.log(friend2);
