const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth");

const router = express.Router();

router.post("/createProduct", auth.verify, (req, res) => {
  let authData = auth.decode(req.headers.authorization);
  productController.createProduct(req.body, authData).then((result) => res.send(result));
});

router.get("/all", (req, res) => {
  productController.allProducts().then((result) => res.send(result));
});

router.get("/allActive", (req, res) => {
  productController.allActive().then((result) => res.send(result));
});

router.get("/:id", (req, res) => {
  productController.specificProduct(req.params).then((result) => res.send(result));
});

router.put("/:id/update", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  productController
    .updateProduct(req.params, req.body, authData)
    .then((result) => res.send(result));
});

router.put("/:id/archive", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  productController.archivedProduct(req.params, authData).then((result) => res.send(result));
});

router.put("/:id/activate", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  productController.activateProduct(req.params, authData).then((result) => res.send(result));
});

module.exports = router;
