const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((result) => res.send(result));
});

router.post("/login", (req, res) => {
  userController.loginUser(req.body).then((result) => res.send(result));
});

router.get("/details", auth.verify, (req, res) => {
  // Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header
  const userData = auth.decode(req.headers.authorization);

  // Provides the user's ID for the getProfile controller method
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/:id", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  userController.toAdmin(req.params, authData).then((result) => res.send(result));
});

module.exports = router;
