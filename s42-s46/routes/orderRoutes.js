const express = require("express");
const orderController = require("../controllers/orderController");
const auth = require("../auth");

const router = express.Router();

router.post("/new", auth.verify, (req, res) => {
  let authData = auth.decode(req.headers.authorization);
  orderController.createOrder(req.body, authData).then((result) => res.send(result));
});

router.get("/retrieve", auth.verify, (req, res) => {
  let authData = auth.decode(req.headers.authorization);
  orderController.retrieveUserOrders(authData).then((result) => res.send(result));
});

router.get("/all", auth.verify, (req, res) => {
  let authData = auth.decode(req.headers.authorization);
  orderController.allOrders(authData).then((result) => res.send(result));
});

//Add to Cart
router.post("/:id", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  let data = {
    userId: authData.id,
    productId: req.body.productId,
    quantity: req.body.quantity,
  };

  orderController.addToCart(data, req.params).then((result) => res.send(result));
});

module.exports = router;
