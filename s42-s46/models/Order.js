const mongoose = require("mongoose");
const Product = require("../models/Product");
const User = require("../models/User");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: User,
    required: [true, "UserId is required"],
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Product,
        required: [true, "ProductId is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total Amount is required"],
  },
  purchasedOn: {
    type: Date,
    default: () => Date.now(),
  },
});

orderSchema.methods.getTotalAmount = function () {
  let data = this.populate("products.productId");
  let total = this.quantity;
  return data;
};

module.exports = mongoose.model("Order", orderSchema);
